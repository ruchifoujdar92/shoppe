package com.shope

import android.R.id
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.badge.BadgeDrawable
import com.shope.api.RetrofitService
import com.shope.databinding.ActivityMainBinding
import com.shope.repository.MainRepository
import com.shope.ui.basket.BasketViewModelFactory
import com.shope.ui.catalogue.CatalogueViewModelFactory
import com.shope.viewmodel.BasketViewModel
import com.shope.viewmodel.CatalogueViewModel
import android.R.id.message
import android.content.IntentFilter

import android.util.Log
import com.shope.ui.catalogue.addQuantity
import java.util.ArrayList


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val retrofitService = RetrofitService.getInstance()
    private lateinit var mainRepository : MainRepository
    private lateinit var badge:BadgeDrawable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        mainRepository= MainRepository(retrofitService,this)
        registerReceiver(datarecevier, IntentFilter("BASKET_QUANTITY_UPDATE"));
        val navView: BottomNavigationView = binding.navView
        badge = navView.getOrCreateBadge(R.id.navigation_basket)
        badge.isVisible = true
        val navController = findNavController(R.id.nav_host_fragment_activity_main)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_catalogue, R.id.navigation_wishlist, R.id.navigation_basket
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
    }
    override fun onResume() {
        super.onResume()
        val totalQuantity =  addQuantity(mainRepository.getBasket(true))
        badge.number = totalQuantity
        // An icon only badge will be displayed unless a number is set:

    }
    private val datarecevier: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val totalQuantity = intent.getStringExtra("total_quantity")
            badge.number = Integer.parseInt(totalQuantity)
        }
    }
    override fun onDestroy() {
       unregisterReceiver(datarecevier)
        super.onDestroy()
    }

    override fun onPause() {
        unregisterReceiver(datarecevier)
        super.onPause()
    }
}