package com.shope.database

import androidx.room.*
import com.shope.model.Products

@Dao
interface ProductDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertProduct(products: Products)

    @Query("Select * from product where basket = :flag")
    fun getBasket(flag:Boolean): List<Products>

    @Query("Select * from product where wishlist = :flag")
    fun getWishList(flag:Boolean): List<Products>

    @Query("DELETE FROM product WHERE productId = :productId")
    fun deleteProduct(productId: String?)

    @Query("Select productId from product where productId = :productId and wishlist = :flag")
    fun isProductExitsWishList(productId: String?,flag: Boolean): Boolean

    @Query("Select productId from product where productId = :productId and basket = :flag")
    fun isProductExitsBaskList(productId: String?,flag: Boolean): Boolean

    @Query("Select quantity from product where productId = :productId")
    fun getQuantity(productId: String?): String

    @Query("UPDATE product SET quantity = :quantity WHERE productId = :productId")
    fun updateQuantity(quantity: String?, productId: String?)

}