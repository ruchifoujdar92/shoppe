package com.shope.repository

import android.content.Context
import com.shope.api.RetrofitService
import com.shope.database.AppDatabase
import com.shope.database.ProductDao
import com.shope.model.Products

class MainRepository(private val retrofitService: RetrofitService?, context: Context?) {

   private var appDatabase: ProductDao = AppDatabase.getInstance(context)?.productDao()!!

    suspend fun getAllItems() = retrofitService?.getAllItems()

    // Insert product
    fun insertProduct(products: Products) = appDatabase.insertProduct(products)

    //Fetch All the products in basket
    fun getBasket(flag:Boolean): List<Products> = appDatabase.getBasket(flag)

   //Fetch All the products in wishlist
   fun getWishList(flag:Boolean): List<Products> = appDatabase.getWishList(flag)

    // Delete product
    fun deleteProduct(productId: String?) = appDatabase.deleteProduct(productId)

    //check if product already exits or not
    fun isProductExitsWishList(productId: String?,flag: Boolean): Boolean
    = appDatabase.isProductExitsWishList(productId,flag)

     //check if product already exits or not
     fun isProductExitsBaskList(productId: String?,flag: Boolean): Boolean =
      appDatabase.isProductExitsBaskList(productId,flag)

    //fetch quantity for a specified product
    fun getQuantity(productId: String?): String = appDatabase.getQuantity(productId)

    //update quantity for product
    fun updateQuantity(quantity: String?,productId: String?) = appDatabase.updateQuantity(quantity,productId)
}

