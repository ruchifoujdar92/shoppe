package com.shope.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.shope.model.Products
import com.shope.repository.MainRepository
import kotlinx.coroutines.*

class BasketViewModel constructor(private val mainRepository: MainRepository): ViewModel() {

    val errorMessage = MutableLiveData<String>()
    val basketList = MutableLiveData<List<Products>>()
    private var job: Job? = null
    private var jobDeleteWishList: Job? = null
    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        onError("Exception handled: ${throwable.localizedMessage}")
    }

    fun getAllBasketList(flag:Boolean) {
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            val allBasketListItems = mainRepository.getBasket(flag)
            withContext(Dispatchers.Main) {
                basketList.postValue(allBasketListItems)
            }
        }
    }
    fun deleteProduct(itemId:String) {
        jobDeleteWishList = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            mainRepository.deleteProduct(itemId)
            withContext(Dispatchers.Main) {
            }
        }
    }
    private fun onError(message: String) {
        errorMessage.postValue(message)
    }

    override fun onCleared() {
        super.onCleared()
        job?.cancel()
        jobDeleteWishList?.cancel()
    }
}