package com.shope.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.shope.model.Products
import com.shope.repository.MainRepository
import kotlinx.coroutines.*

class WishListViewModel constructor(private val mainRepository: MainRepository): ViewModel() {

    val errorMessage = MutableLiveData<String>()
    val wishList = MutableLiveData<List<Products>>()
    private var jobWishList: Job? = null
    private var jobDeleteWishList: Job? = null
    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        onError("Exception handled: ${throwable.localizedMessage}")
    }

    fun getAllWishList(flag:Boolean) {
        jobWishList = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            val allWishListItems = mainRepository.getWishList(flag)
            withContext(Dispatchers.Main) {
                wishList.postValue(allWishListItems)
            }
        }
    }

    fun deleteProduct(productId:String) {
        jobDeleteWishList = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            mainRepository.deleteProduct(productId)
            withContext(Dispatchers.Main) {
            }
        }
    }

    private fun onError(message: String) {
        errorMessage.postValue(message)
    }

    override fun onCleared() {
        super.onCleared()
        jobWishList?.cancel()
        jobDeleteWishList?.cancel()
    }
}