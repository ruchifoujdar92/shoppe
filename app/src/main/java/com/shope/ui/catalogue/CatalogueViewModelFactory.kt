package com.shope.ui.catalogue

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.shope.repository.MainRepository
import com.shope.viewmodel.CatalogueViewModel

class CatalogueViewModelFactory constructor(private val repository: MainRepository): ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(CatalogueViewModel::class.java)) {
            CatalogueViewModel(this.repository) as T
        } else {
            throw IllegalArgumentException("CatalogueViewModel Not Found")
        }
    }
}