package com.shope.ui.catalogue

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.shope.adapter.CatalogueAdapter
import com.shope.api.RetrofitService
import com.shope.repository.MainRepository
import androidx.databinding.DataBindingUtil
import com.shope.R
import com.shope.databinding.CustomDialogFragmentBinding
import com.shope.databinding.FragmentCatalogueBinding
import com.shope.model.Products
import com.shope.viewmodel.CatalogueViewModel
import kotlinx.coroutines.*


class CatalogueFragment : Fragment() {

    private lateinit var catalogueViewModel: CatalogueViewModel
    private var _binding: FragmentCatalogueBinding? = null
    private val catalogueAdapter = CatalogueAdapter()
    private val retrofitService = RetrofitService.getInstance()
    private lateinit var mainRepository : MainRepository
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        mainRepository= MainRepository(retrofitService,context)
        catalogueViewModel = ViewModelProvider(this,
            CatalogueViewModelFactory(mainRepository)
        )[CatalogueViewModel::class.java]

        _binding = FragmentCatalogueBinding.inflate(inflater, container, false)
        val root: View = binding.root
        binding.recyclerview.adapter = catalogueAdapter

        binding.retryBtn.visibility = View.INVISIBLE
        catalogueViewModel.itemList.observe(viewLifecycleOwner) {
            catalogueAdapter.setItems(it)
            binding.retryBtn.visibility = View.INVISIBLE
        }

        catalogueViewModel.errorMessage.observe(viewLifecycleOwner) {
            Toast.makeText(context, it, Toast.LENGTH_SHORT).show()
            binding.retryBtn.visibility = View.VISIBLE
        }

        catalogueViewModel.loading.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.progressDialog.visibility = View.VISIBLE
            } else {
                binding.progressDialog.visibility = View.GONE
            }
        })
        catalogueViewModel.getAllItems()

        catalogueAdapter.setOnItemClickListener(object : CatalogueAdapter.ClickListener {
            override fun onItemClick(v: View, position: Int) {
                val productDetails =  catalogueViewModel.itemList.value?.products?.get(position)
                if (productDetails != null) {
                    showCustomAlert(productDetails)
                }
            }
        })

        binding.retryBtn.setOnClickListener{
            catalogueViewModel.getAllItems()
        }
        return root
    }
    private fun showCustomAlert(productDetails:Products) {
        val binding: CustomDialogFragmentBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.custom_dialog_fragment,
                null,
                false
            )
        val customDialog = context?.let {
            AlertDialog.Builder(it)
                .setView(binding.root)
                .show()
        }

        binding.productList = productDetails
        binding.cancelBt.setOnClickListener {
            customDialog?.dismiss()
        }
        customDialog?.setCanceledOnTouchOutside(false)
        binding.basketBtn.setOnClickListener {
            productDetails?.basket = true
            productDetails?.wishlist = false
            if (mainRepository.isProductExitsBaskList(productDetails?.productId, true)) {
                var first = mainRepository.getQuantity(productDetails?.productId)
                if (first == null) {
                    first = 0.toString()
                }
                var sum = sum(Integer.parseInt(first), 1)
                productDetails?.quantity = sum.toString()
                if(productDetails.quantity!! <= productDetails.stock.toString()){
                    mainRepository.updateQuantity(sum.toString(), productDetails.productId)
                    Toast.makeText(context, "Product added in cart", Toast.LENGTH_SHORT).show()
                }
                else{
                    Toast.makeText(context, "Not enough product in stock", Toast.LENGTH_SHORT).show()
                }
            } else {
                productDetails?.quantity = "1"
                if(productDetails.quantity!! <= productDetails.stock.toString()){
                    mainRepository.insertProduct(productDetails)
                    Toast.makeText(context, "Product added in cart", Toast.LENGTH_SHORT).show()
                }
                else{
                    Toast.makeText(context, "Not enough product in stock", Toast.LENGTH_SHORT).show()
                }

            }
            val totalQuantity =  addQuantity(mainRepository.getBasket(true))
            val intent = Intent("BASKET_QUANTITY_UPDATE")
            intent.putExtra("total_quantity", totalQuantity.toString())
            context?.sendBroadcast(intent)
        }

        binding.whishlistBtn.setOnClickListener {
            productDetails?.wishlist = true
            productDetails?.basket = false
            if ((!mainRepository.isProductExitsWishList(productDetails?.productId, true))) {
                mainRepository.insertProduct(productDetails)
                Toast.makeText(context, "Product added in wishlist", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(context, "Product already in wishlist", Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}

fun sum (first:Int, second: Int): Int {
   return first + second
}

fun subtract (first:Int, second: Int): Int {
    if (first==0 || first==null){
        return 0
    }
    return first - second
}

fun addQuantity(productList: List<Products>): Int {
    var quantity: Int
    var sum = 0
    if (productList != null) {
        for (s in productList) {
            quantity = Integer.parseInt(s.quantity)
            sum = sum.plus(quantity)
        }
    }
    return sum
}
