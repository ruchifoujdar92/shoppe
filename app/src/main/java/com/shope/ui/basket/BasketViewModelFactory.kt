package com.shope.ui.basket

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.shope.repository.MainRepository
import com.shope.viewmodel.BasketViewModel
import com.shope.viewmodel.WishListViewModel

class BasketViewModelFactory constructor(private val repository: MainRepository): ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(BasketViewModel::class.java)) {
            BasketViewModel(this.repository) as T
        } else {
            throw IllegalArgumentException("BasketViewModel Not Found")
        }
    }
}