package com.shope.ui.basket

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.shope.adapter.BasketAdapter
import com.shope.api.RetrofitService
import com.shope.databinding.FragmentBasketBinding
import com.shope.model.Products
import com.shope.repository.MainRepository
import com.shope.ui.catalogue.addQuantity
import com.shope.util.SwipeToDelete
import com.shope.viewmodel.BasketViewModel

class BasketFragment : Fragment() {

    private lateinit var basketViewModel: BasketViewModel
    private var _binding: FragmentBasketBinding? = null
    private val basketAdapter = BasketAdapter()
    private val retrofitService = RetrofitService.getInstance()
    private lateinit var mainRepository : MainRepository
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mainRepository= MainRepository(retrofitService,context)
        basketViewModel = ViewModelProvider(this,
            BasketViewModelFactory(mainRepository)
        )[BasketViewModel::class.java]

        _binding = FragmentBasketBinding.inflate(inflater, container, false)
        val root: View = binding.root
        binding.recyclerview.adapter = basketAdapter

        context?.registerReceiver(dataReceiverUpdateBasket, IntentFilter("UPDATE_BASKET_AFTER_DELETE_ADD"));

        basketViewModel.basketList.observe(viewLifecycleOwner) {
            basketAdapter.setProducts(it)
            val list = basketViewModel.basketList.value
            val total = getTotalAmount(list)
            binding.totalTv.text = "$$total"
        }

        basketViewModel.errorMessage.observe(viewLifecycleOwner) {
            Toast.makeText(context, it, Toast.LENGTH_SHORT).show()
        }

        basketViewModel.getAllBasketList(true)

        enableSwipeToDelete()
        return root
    }

    //swipe to delete functionality
    private fun enableSwipeToDelete() {
        val item = object : SwipeToDelete(0, ItemTouchHelper.LEFT){
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.bindingAdapterPosition
                val productId = basketViewModel.basketList.value?.get(position)?.productId
                if (productId != null) {
                    basketViewModel.deleteProduct(productId)
                    val totalQuantity =  addQuantity(mainRepository.getBasket(true))
                    val intent = Intent("BASKET_QUANTITY_UPDATE")
                    intent.putExtra("total_quantity", totalQuantity.toString())
                    context?.sendBroadcast(intent)
                    basketViewModel.getAllBasketList(true)
                }

            }
        }
        ItemTouchHelper(item).attachToRecyclerView(binding.recyclerview)
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
        context?.unregisterReceiver(dataReceiverUpdateBasket)
    }

    private val dataReceiverUpdateBasket: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val totalAmount = intent.getStringExtra("TOTAL_AMOUNT")
            val call = intent.getStringExtra("CALL")
            if (call.equals("deleteProduct")){
                basketViewModel.getAllBasketList(true)
            }
            binding.totalTv.text = "$$totalAmount"

        }
    }
}
fun getTotalAmount(list: List<Products>?):Double{
    var sum = 0.0
    var price: Double
    var newPrice: Double
    var  quantity: Int;
    if (list != null) {
        for (s in list) {
            price = s.price?.toDouble() ?: 0.0
            quantity = Integer.parseInt(s.quantity)
            newPrice = quantity * price
            sum = sum.plus(newPrice)
        }
    }
    return sum
}
