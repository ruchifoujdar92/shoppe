package com.shope.ui.wishlist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.shope.repository.MainRepository
import com.shope.viewmodel.WishListViewModel

class WishlistViewModelFactory constructor(private val repository: MainRepository): ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(WishListViewModel::class.java)) {
            WishListViewModel(this.repository) as T
        } else {
            throw IllegalArgumentException("WishListViewModel Not Found")
        }
    }
}