package com.shope.ui.wishlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.shope.adapter.WishlistAdapter
import com.shope.api.RetrofitService
import com.shope.databinding.FragmentWishlistBinding
import com.shope.repository.MainRepository
import com.shope.util.SwipeToDelete
import com.shope.viewmodel.WishListViewModel

class WishListFragment : Fragment() {

    private lateinit var wishListViewModel: WishListViewModel
    private var _binding: FragmentWishlistBinding? = null
    private val wishlistAdapter = WishlistAdapter()
    private val retrofitService = RetrofitService.getInstance()
    private lateinit var mainRepository : MainRepository

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        mainRepository= MainRepository(retrofitService,context)
        wishListViewModel = ViewModelProvider(this,
            WishlistViewModelFactory(mainRepository)
        )[WishListViewModel::class.java]

        _binding = FragmentWishlistBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.recyclerview.adapter = wishlistAdapter

        wishListViewModel.wishList.observe(viewLifecycleOwner) {
            wishlistAdapter.setProducts(it)
        }

        wishListViewModel.errorMessage.observe(viewLifecycleOwner) {
            Toast.makeText(context, it, Toast.LENGTH_SHORT).show()
        }

        wishListViewModel.getAllWishList(true)

        enableSwipeToDelete()
        return root
    }
    //swipe to delete functionality
    private fun enableSwipeToDelete() {
        val item = object : SwipeToDelete(0,ItemTouchHelper.LEFT){
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.adapterPosition
                val productId = wishListViewModel.wishList.value?.get(position)?.productId
                if (productId != null) {
                    wishListViewModel.deleteProduct(productId)
                    wishListViewModel.getAllWishList(true)
                }
            }
        }
        ItemTouchHelper(item).attachToRecyclerView(binding.recyclerview)
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}