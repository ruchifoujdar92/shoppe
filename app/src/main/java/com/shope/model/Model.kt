package com.shope.model

import android.os.Build
import android.os.Parcel
import android.os.Parcelable
import com.bumptech.glide.Glide

import android.widget.ImageView
import androidx.annotation.RequiresApi

import androidx.databinding.BindingAdapter
import androidx.room.Entity
import androidx.room.PrimaryKey

data class Item(val products : ArrayList<Products> = arrayListOf())

@BindingAdapter("image")
fun loadImage(view: ImageView, imageUrl: String?) {
    Glide.with(view.context).load(imageUrl).into(view)
}

@Entity(tableName = "product")
data class Products(@PrimaryKey(autoGenerate = true) var itemId: Int,
                    val name: String?, val image: String?, val price: String?,
                    val stock: String?, val category: String?, val oldPrice:
                    String?, val productId: String?, var basket:Boolean?,
                    var wishlist: Boolean?, var quantity: String?): Parcelable {
    @RequiresApi(Build.VERSION_CODES.Q)
    constructor(parcel: Parcel) : this(
        parcel.readValue(Int::class.java.classLoader) as Int,
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readBoolean(),
        parcel.readBoolean(),
        parcel.readString()
    )

    override fun describeContents(): Int {
        TODO("Not yet implemented")
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(itemId)
        parcel.writeValue(name)
        parcel.writeValue(image)
        parcel.writeValue(price)
        parcel.writeValue(stock)
        parcel.writeValue(category)
        parcel.writeValue(oldPrice)
        parcel.writeValue(productId)
        parcel.writeValue(basket)
        parcel.writeValue(wishlist)
        parcel.writeValue(quantity)
    }

    companion object CREATOR : Parcelable.Creator<Products> {
        @RequiresApi(Build.VERSION_CODES.Q)
        override fun createFromParcel(parcel: Parcel): Products {
            return Products(parcel)
        }

        override fun newArray(size: Int): Array<Products?> {
            return arrayOfNulls(size)
        }
    }
}

