package com.shope.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.shope.api.RetrofitService
import com.shope.databinding.AdapterBasketBinding
import com.shope.model.Products
import com.shope.repository.MainRepository
import com.shope.ui.basket.getTotalAmount
import com.shope.ui.catalogue.addQuantity
import com.shope.ui.catalogue.subtract
import com.shope.ui.catalogue.sum

class BasketAdapter : RecyclerView.Adapter<BasketAdapter.MainViewHolder>() {

    private lateinit var productDetails: Products
    var productList = listOf<Products>()
    private lateinit var mainRepository : MainRepository

    fun setProducts(products: List<Products>) {
        this.productList = products
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        mainRepository= MainRepository(RetrofitService.retrofitService,parent.context)
        val binding = AdapterBasketBinding.inflate(inflater, parent, false)
        return MainViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        productDetails = productList[position]
        holder.bind(productDetails)
        holder.binding.addQuantityBtn.setOnClickListener{
            var productId = productList[position].productId
            productList[position].basket = true
            productList[position].wishlist = false
            var first = mainRepository.getQuantity(productId)
            if (first == null){
                first = 0.toString()
            }
            var sum = sum(Integer.parseInt(first),1)
            productList[position]?.quantity = sum.toString()
            if(productList[position].quantity!! <= productList[position].stock.toString()){
                mainRepository.updateQuantity(sum.toString(),productId)
                productList[position].quantity = sum.toString()
                notifyDataSetChanged()
                sendBasketBroadCast(holder)
            }
            else{
                Toast.makeText(holder.itemView.context, "Not enough product in stock", Toast.LENGTH_SHORT).show()
            }

        }
        holder.binding.subtractQuantityBtn.setOnClickListener {
            var productId = productList[position].productId
            productList[position].basket = true
            productList[position].wishlist = false
            var first = mainRepository.getQuantity(productId)
            if (first == null){
                first = 0.toString()
            }
            var subtract = subtract(Integer.parseInt(first), 1)
            if (subtract == 0) {
                mainRepository.deleteProduct(productId)
            } else {
                productList[position]?.quantity = subtract.toString()
                mainRepository.updateQuantity(subtract.toString(), productId)
            }
            notifyDataSetChanged()
            sendBasketBroadCast(holder)
        }


    }

    override fun getItemCount(): Int {
        return productList.size
    }

    class MainViewHolder(val binding: AdapterBasketBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(products: Products) {
            binding.productList= products
        }

    }

    private fun sendBasketBroadCast(holder: MainViewHolder) {
        val intentQuantityUpdate = Intent("BASKET_QUANTITY_UPDATE")
        val totalQuantity =  addQuantity(mainRepository.getBasket(true))
        intentQuantityUpdate.putExtra("total_quantity", totalQuantity.toString())
        holder.itemView.context.sendBroadcast(intentQuantityUpdate)

        val totalAmount = getTotalAmount(productList)
        val intentUpdateAfterDelete = Intent("UPDATE_BASKET_AFTER_DELETE_ADD")
        intentUpdateAfterDelete.putExtra("TOTAL_AMOUNT", totalAmount.toString())
        intentUpdateAfterDelete.putExtra("CALL", "deleteProduct")
        holder.itemView.context.sendBroadcast(intentUpdateAfterDelete)

    }
}
