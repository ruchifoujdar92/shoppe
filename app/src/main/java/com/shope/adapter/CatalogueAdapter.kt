package com.shope.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.shope.databinding.AdapterCatalogueBinding
import com.shope.model.Item
import com.shope.model.Products

class CatalogueAdapter : RecyclerView.Adapter<CatalogueAdapter.MainViewHolder>() {

    private var itemList = Item()
    fun setItems(items: Item) {
        this.itemList = items
        notifyDataSetChanged()
    }

    private var clickListener: ClickListener? = null

   inner class MainViewHolder(private val binding: AdapterCatalogueBinding) :
       RecyclerView.ViewHolder(binding.root), View.OnClickListener {
       fun bind(products: Products) {
           binding.productList= products
       }
       init {
           if (clickListener != null) {
               itemView.setOnClickListener(this)
           }
       }
         override fun onClick(v: View?) {
              if (v != null) {
                  clickListener?.onItemClick(v,adapterPosition)
              }
          }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {

        val inflater = LayoutInflater.from(parent.context)
        val binding = AdapterCatalogueBinding.inflate(inflater, parent, false)

        return MainViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        val productDetails = itemList.products[position]
        holder.bind(productDetails)
          }
    fun setOnItemClickListener(clickListener: ClickListener) {
        this.clickListener = clickListener
    }

    override fun getItemCount(): Int {
        return itemList.products.size
    }
    interface ClickListener {
        fun onItemClick(v: View,position: Int)
    }
}
