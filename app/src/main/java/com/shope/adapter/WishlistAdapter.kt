package com.shope.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.shope.api.RetrofitService.Companion.retrofitService
import com.shope.databinding.AdapterWishlistBinding
import com.shope.model.Products
import com.shope.repository.MainRepository
import com.shope.ui.catalogue.addQuantity
import com.shope.ui.catalogue.sum

class WishlistAdapter : RecyclerView.Adapter<WishlistAdapter.MainViewHolder>() {

    var productList = listOf<Products>()
    private lateinit var mainRepository : MainRepository
    private lateinit var productDetails : Products

    fun setProducts(products: List<Products>) {
        this.productList = products
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        mainRepository= MainRepository(retrofitService,parent.context)
        val binding = AdapterWishlistBinding.inflate(inflater, parent, false)
        return MainViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        productDetails = productList[position]
        holder.bind(productDetails)
        holder.binding.addBasketBtn.setOnClickListener{
            productList[position].basket = true
            productList[position].wishlist = true
            var productId = productList[position].productId
            if(mainRepository.isProductExitsBaskList(productId,true)) {
                var first = mainRepository.getQuantity(productId)
                if (first == null){
                    first = 0.toString()
                }
                var sum = sum(Integer.parseInt(first),1)
                productList[position]?.quantity = sum.toString()
                if(productList[position].quantity!! <= productList[position].stock.toString()){
                    mainRepository.updateQuantity(sum.toString(),productId)
                }else{
                    Toast.makeText(holder.itemView.context, "Not enough product in stock", Toast.LENGTH_SHORT).show()
                }
            }
            else{
                productList[position]?.quantity = "1"
                if(productList[position].quantity!! <= productList[position].stock.toString()){
                    mainRepository.insertProduct(productDetails)
                }
                else{
                    Toast.makeText(holder.itemView.context, "Not enough product in stock", Toast.LENGTH_SHORT).show()
                }
            }
            val totalQuantity =  addQuantity(mainRepository.getBasket(true))
            val intent = Intent("BASKET_QUANTITY_UPDATE")
            intent.putExtra("total_quantity", totalQuantity.toString())
            holder.itemView.context.sendBroadcast(intent)
        }
    }

    override fun getItemCount(): Int {
        return productList.size
    }

    class MainViewHolder(val binding: AdapterWishlistBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(products: Products) {
            binding.productList= products
        }
    }
}
