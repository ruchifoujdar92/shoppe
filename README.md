# Shoppe

Your one stop destination


Shoppe Application is your one stop destination to your dreamy shopping experience.As a user you can add new products to your cart or wishlist them for you to buy later.Making shopping experience for the user smooth and easy.
Followed best UI practises.

Application has three different screens:
1) Fragment Catalogue where user can see list of products displayed
2) Fragment WishList which shows products added to buy later as wishlist
3) Fragment Basket which contains product to purchase

Using API for getting list of products(https://api.npoint.io/0f78766a6d68832d309d/)
Using broadcast recevier to update values for botton navigation badge count

Using CRUD operations.
1) Swipe to delete functionality implemented in WishList and Basket Fragment
2) Updating using UPDATE query when calling update quantity details for a product
3) INSERT query for inserting product details for a particular product added to cart or wishlist
